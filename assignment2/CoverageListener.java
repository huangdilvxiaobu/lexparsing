package gov.nasa.jpf.listener;

import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

import java.util.Arrays;
import java.util.HashMap;

public class CoverageListener extends ListenerAdapter {

    HashMap<Integer, String> map = new HashMap<>();
    @Override
    public void instructionExecuted(VM vm, ThreadInfo currentThread, Instruction nextInstruction, Instruction executedInstruction) {
        String ins = executedInstruction.getSourceLine();
        int lineNumber = executedInstruction.getLineNumber();
        if (ins != null) {
            map.putIfAbsent(lineNumber, ins);
        }
//        if(nextInstruction.isCompleted(currentThread)) {
//            Object[] key = map.keySet().toArray();
//            Arrays.sort(key);
//            for (int i = 0; i < key.length; i++) {
//                System.out.println(i);
//                System.out.println(map.get(key[i]));
//            }
//        }
    }
    public void threadTerminated(VM vm, ThreadInfo terminatedThread) {
        Object[] key = map.keySet().toArray();
        Arrays.sort(key);
        for (int i = 0; i < key.length; i++) {
            System.out.println(key[i] + " " + map.get(key[i]));
        }
    }



}
