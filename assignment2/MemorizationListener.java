package gov.nasa.jpf.listener;

import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.vm.*;

import java.util.HashMap;

public class MemorizationListener extends ListenerAdapter {

    HashMap<String,Object> map = new HashMap<>();
    String key = null;
    @Override
    public void methodEntered (VM vm, ThreadInfo currentThread, MethodInfo enteredMethod) {
     //you will have to traverse heap starting from both "this" and all the arguments give to the method.

        //todo need to filter some system function calls first
        Heap heap = vm.getHeap();
        ClassInfo classInfo = enteredMethod.getClassInfo();
        ElementInfo elementInfo = heap.get(classInfo.getClassObjectRef());
        String className = classInfo.getName();
        String key = className + enteredMethod.getUniqueName(); //class name+ method name as key
        StackFrame stackFrame = currentThread.getTopFrame();
        LocalVarInfo[] arguments = enteredMethod.getArgumentLocalVars();
        if(arguments!=null) {
            for (int i =0 ; i< arguments.length; i++) {
                if (stackFrame.isReferenceSlot(i)) {
                    tranverse(elementInfo,classInfo.getClassObjectRef(),heap);
                } else {
                    key += stackFrame.getSlot(i);  //store the value of each argument as part of key
                }
            }
        }
        if(map.containsKey(key)){
            enteredMethod.setReturnValue(map.get(key));   //todo need to set the return value of method and directly return
        }
    }
    public void methodExited (VM vm, ThreadInfo currentThread, MethodInfo exitedMethod) {
        //todo need to filter the system function calls first
        StackFrame stackFrame = currentThread.getTopFrame();
        map.putIfAbsent(key,stackFrame.peek());  //put the return value of this method into map
        key = null;
    }
    public void tranverse(ElementInfo ei,int refid,Heap heap){
        Fields fields = ei.getFields();
        FieldInfo fi = ei.getFieldInfo(refid);
        if(fi.isReference()){
            //point to the next reference object
            ClassInfo ci = fi.getClassInfo();
            tranverse(heap.get(ci.getClassObjectRef()),ci.getClassObjectRef(),heap);
        }
        key += fields.toString();
    }


}
