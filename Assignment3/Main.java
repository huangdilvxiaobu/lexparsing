import jdk.internal.org.objectweb.asm.ClassReader;
import jdk.internal.org.objectweb.asm.ClassWriter;

import java.io.*;

public class Main {

    public static void main(String[] args) {

        try {
            // 1. load .class file that we want to modify
            InputStream inputStream = new FileInputStream("C:\\Users\\52741\\IdeaProjects\\CFG_Assignment\\src\\test.class");
            ClassReader cr = new ClassReader(inputStream);
            ClassWriter cw = new ClassWriter(cr, 0);
            PrintStream out = new PrintStream("C:\\Users\\52741\\IdeaProjects\\CFG_Assignment\\src\\result.txt");
            System.setOut(out);

            // 2. modify the IR
            TraceVisitor tv = new TraceVisitor(cw);
            cr.accept(tv, 0);


        }
        catch (Exception e){}
    }
}
