import jdk.internal.org.objectweb.asm.*;


import java.util.*;

public class CFGVistor extends MethodVisitor {

    private String methodName;
    private List<String> startOfBlock = new ArrayList<>();
    private HashMap<String, Integer> labelList = new LinkedHashMap<>();

    public CFGVistor(MethodVisitor mv, String methodName) {
        super(Opcodes.ASM5, mv);
        this.methodName = methodName;
    }


    @Override
    public void visitLabel(Label label) {
        this.labelList.put(label.toString(), this.labelList.getOrDefault(label.toString(), 0) + 1);
        System.out.println("visit label: " + label.toString());
    }

    @Override
    public void visitJumpInsn(int opcode, Label label) {
        this.labelList.put(label.toString(), this.labelList.getOrDefault(label.toString(), 0) + 1);
        this.startOfBlock.add(label.toString());
        System.out.println("Visit jump label " + label.toString());
    }

    @Override
    public void visitLineNumber(int line, Label start) {
        // put labels and corresponding line number into map
        System.out.println("line is " + line + " label is : " + start);
    }

    @Override
    public void visitEnd() {
        Iterator<Map.Entry<String, Integer>> iterator = labelList.entrySet().iterator();
        startOfBlock.add(iterator.next().getKey());
        Map.Entry<String, Integer> entry = iterator.next();

        while (iterator.hasNext()) {
            if (entry.getValue() > 1) {
                entry = iterator.next();
                startOfBlock.add(entry.getKey());
            } else {
                entry = iterator.next();
            }
        }


        Map<Integer, List<String>> CFG = new HashMap<>();
        int count = 0;
        for (String label : labelList.keySet()) {
            if (startOfBlock.contains(label)) { //find a new block
                List<String> targetList = new ArrayList<>();
                targetList.add(label);
                if(count==2) {
                    CFG.get(count - 1).add(label);
                }
                CFG.put(count, targetList);
                count++;
            }
        }


        System.out.println("Method is: " + this.methodName);
        System.out.println("Basic Block information");
        for (Map.Entry<Integer, List<String>> BasicBlock : CFG.entrySet()) {
            if (BasicBlock.getKey() != 0) {
                System.out.println("Basic Block id: " + BasicBlock.getKey());
                for (int i = 0; i < BasicBlock.getValue().size(); i++)
                    System.out.println("Target label is : " + BasicBlock.getValue().get(i));
            }
        }
    }
}


