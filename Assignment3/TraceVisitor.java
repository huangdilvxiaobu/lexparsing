import jdk.internal.org.objectweb.asm.*;


class TraceVisitor extends ClassVisitor {

    public TraceVisitor(ClassWriter cw) {
        super(Opcodes.ASM5, cw);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        MethodVisitor mv = super.visitMethod(access, name, desc, signature, exceptions);
        if (!name.equals("<init>")) {
            mv = new CFGVistor(mv, name);
        }
        return mv;
    }
}
