/* Generated By:JavaCC: Do not edit this line. SyntaxCheckerConstants.java */

/**
 * Token literal values and constants.
 * Generated by org.javacc.parser.OtherFilesGen#start()
 */
public interface SyntaxCheckerConstants {

  /** End of File. */
  int EOF = 0;
  /** RegularExpression Id. */
  int PROGRAM = 24;
  /** RegularExpression Id. */
  int BREAK = 25;
  /** RegularExpression Id. */
  int CLASS = 26;
  /** RegularExpression Id. */
  int INTERFACE = 27;
  /** RegularExpression Id. */
  int ENUM = 28;
  /** RegularExpression Id. */
  int ELSE = 29;
  /** RegularExpression Id. */
  int CONST = 30;
  /** RegularExpression Id. */
  int IF = 31;
  /** RegularExpression Id. */
  int NEW = 32;
  /** RegularExpression Id. */
  int PRINT = 33;
  /** RegularExpression Id. */
  int READ = 34;
  /** RegularExpression Id. */
  int RETURN = 35;
  /** RegularExpression Id. */
  int VOID = 36;
  /** RegularExpression Id. */
  int FOR = 37;
  /** RegularExpression Id. */
  int EXTENDS = 38;
  /** RegularExpression Id. */
  int IMPLEMENTS = 39;
  /** RegularExpression Id. */
  int CONTINUE = 40;
  /** RegularExpression Id. */
  int SEMIC = 41;
  /** RegularExpression Id. */
  int COLON = 42;
  /** RegularExpression Id. */
  int LBR = 43;
  /** RegularExpression Id. */
  int RBR = 44;
  /** RegularExpression Id. */
  int COMMA = 45;
  /** RegularExpression Id. */
  int MINUS = 46;
  /** RegularExpression Id. */
  int RCBR = 47;
  /** RegularExpression Id. */
  int LCBR = 48;
  /** RegularExpression Id. */
  int LSBR = 49;
  /** RegularExpression Id. */
  int RSBR = 50;
  /** RegularExpression Id. */
  int INT = 51;
  /** RegularExpression Id. */
  int CHAR = 52;
  /** RegularExpression Id. */
  int BOOL = 53;
  /** RegularExpression Id. */
  int ID = 54;
  /** RegularExpression Id. */
  int numConst = 55;
  /** RegularExpression Id. */
  int charConst = 56;
  /** RegularExpression Id. */
  int boolConst = 57;
  /** RegularExpression Id. */
  int DIGIT = 58;
  /** RegularExpression Id. */
  int LETTER = 59;

  /** Lexical state. */
  int DEFAULT = 0;

  /** Literal token values. */
  String[] tokenImage = {
    "<EOF>",
    "\" \"",
    "\"\\t\"",
    "\"\\n\"",
    "\"\\r\"",
    "\"\\f\"",
    "<token of kind 6>",
    "\"++\"",
    "\"--\"",
    "\"||\"",
    "\"&&\"",
    "\".\"",
    "\"=\"",
    "\"==\"",
    "\"!=\"",
    "\">\"",
    "\">=\"",
    "\"<\"",
    "\"<=\"",
    "\"+\"",
    "\"\\u9225\\ufffd\"",
    "\"*\"",
    "\"/\"",
    "\"%\"",
    "\"program\"",
    "\"break\"",
    "\"class\"",
    "\"interface\"",
    "\"enum\"",
    "\"else\"",
    "\"const\"",
    "\"if\"",
    "\"new\"",
    "\"print\"",
    "\"read\"",
    "\"return\"",
    "\"void\"",
    "\"for\"",
    "\"extends\"",
    "\"implements\"",
    "\"continue\"",
    "\";\"",
    "\":\"",
    "\"(\"",
    "\")\"",
    "\",\"",
    "\"-\"",
    "\"}\"",
    "\"{\"",
    "\"[\"",
    "\"]\"",
    "\"int\"",
    "\"char\"",
    "\"bool\"",
    "<ID>",
    "<numConst>",
    "<charConst>",
    "<boolConst>",
    "<DIGIT>",
    "<LETTER>",
  };

}
